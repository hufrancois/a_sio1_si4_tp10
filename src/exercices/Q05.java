package exercices;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static utilitaires.UtilDate.*;

public class Q05 {

    public static void main(String[] args) throws SQLException {
        
        Connection cxBdd =    connexionBdd() ;
        
        System.out.println("Liste des villes totalisant plus de 8 victoires par des judokas masculins classées par nombres de victoires décroissantes :");
      
        String requete= "SELECT ville, sum(nbVictoires) " +
                        "FROM Personne " +
                        "WHERE sexe = 'M' " +
                        "AND sum(nbVictoires) >= 8 " +
                        "ORDER BY nbVictoires DESC";
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
      
        while (  pers.next() ) {
            
            afficher(pers);
         }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

    private static void afficher(ResultSet pers) throws SQLException {
        
        final String format=" %-15s %-15s %-10s %4d kg %-15s %2d victoires\n";
        System.out.printf(format,
                                      
                          pers.getString("nom"),
                          pers.getString("prenom"),
                          dateVersChaine(pers.getDate("datenaiss")),
                          pers.getInt("poids"),
                          pers.getString("ville"),
                          pers.getInt("nbVictoires"));
    }

    private static Connection connexionBdd() throws SQLException {
 
        final String    URL                  = "jdbc:derby:BddJudo";
        final String    UTILISATEUR          = "uJudo";
        final String    MOTDEPASSE           = "judo";
        
        DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
        Connection cxBdd =    DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE) ;
        return cxBdd;
    }    
}