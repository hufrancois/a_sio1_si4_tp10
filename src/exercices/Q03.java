package exercices;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static utilitaires.UtilDate.*;

public class Q03 {

    public static void main(String[] args) throws SQLException {
        
        Connection cxBdd =    connexionBdd() ;
      
        String requete= "Select avg(poids) " +
                        "FROM Personne " +
                        "WHERE sexe = 'M' ";
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
  
        pers.next();
        
        System.out.println("\n");
         
        System.out.println("Moyenne des poids des judokas masculins :" +pers.getFloat(1)+ " kg.");
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }
 
   

    private static Connection connexionBdd() throws SQLException {
 
        final String    URL                  = "jdbc:derby:BddJudo";
        final String    UTILISATEUR          = "uJudo";
        final String    MOTDEPASSE           = "judo";
        
        DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
        Connection cxBdd =    DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE) ;
        return cxBdd;
    }    
}